Package: vtg.coxphl1
Type: Package
Title: What the Package Does (Title Case)
Version: 0.1.1
Author: Benedetta Gottardelli
Maintainer: The package maintainer <yourself@somewhere.net>
Description: More about what it does (maybe more than one line)
    Use four spaces when indenting paragraphs within the Description.
License: What license is it under?
Encoding: UTF-8
LazyData: true
Imports:
    glue,
    httr,
    rjson,
    vtg
Remotes:
    mellesies/vtg
RoxygenNote: 7.0.0
