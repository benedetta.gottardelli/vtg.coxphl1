#' Compute the aggregate statistic of step 2
#' sum over all distinct times i
#'   sum the covariates of cases in the set of cases with events at time i.
#'
#' Params:
#'   df: dataframe
#'   expl_vars: list of explanatory variables (covariates) to use
#'   time_col: name of the column that contains the event/censor times
#'   censor_col: name of the colunm that explains whether an event occured or
#'               the patient was censored
#'
#' Return:
#'   numeric vector with sums and named index with covariates.
RPC_compute_exp_eta_sum <- function(df, expl_vars, time_col, censor_col, beta, df_time, norm, mean_cols, std_cols){
  print('RPC_compute_exp_eta_sum')
  #install.packages("spatstat.utils")
  #library(spatstat.utils)
  if (norm == TRUE) {
    df <- normalize.data(df, expl_vars, censor_col, time_col, mean_cols, std_cols)
  }
  df$eta = as.matrix(df[, expl_vars])%*% beta
  df$expEta = exp(df$eta)

  R = apply(df_time, MARGIN = 1, function(x) sum(df[df[,time_col] >= as.integer(x['time']), 'expEta']))
  R <- as.data.frame(R)
  rownames(R) <- df_time[,'time']
  #print(R)
  return(R)
}




