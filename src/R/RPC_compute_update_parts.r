#' Return a list with the contributions to the numerator and the demomiator of the update fromula
#'
#' Params:
#'   df: dataframe
#'   time_col: name of the column that contains the event/censor times
#'   censor_col: name of the column that contains the event/censor
#'   beta: parameter vector
#'   df_time: dataframe with columns time and Freq
#'   R:
#'   coord, coord_index: coordinate and coordinate index to update
#'
#' Return:
#'   num numerator contributions
#'   den denominator contribution

RPC_compute_update_parts <- function(df, expl_vars, time_col, censor_col, beta, df_time, R, coord, coord_index, norm, mean_cols, std_cols) {


  if (norm == TRUE) {
    df <- normalize.data(df, expl_vars, censor_col, time_col, mean_cols, std_cols)
  }

  df$eta = as.matrix(df[, expl_vars])%*% beta
  df$expEta = exp(df$eta)
  data_stage <- merge(x = df, y = df_time, by = NULL)
  data_stage <- data_stage[data_stage[, 'time'] <= data_stage[, time_col],]
  data_stage <- merge(x = data_stage, y = R, by.x = 'time', by.y = 'row.names' )
  data_stage$w <- data_stage$freq*(data_stage$expEta*data_stage$R+data_stage$expEta**2)/data_stage$R**2
  data_stage$prik = data_stage$freq*data_stage$expEta/data_stage$R

  cols <- names(df)
  df_w <- aggregate(data_stage$w, by=data_stage[cols], sum)
  names(df_w) <- c(cols, 'w')

  df_prik <- aggregate(data_stage$prik, by=data_stage[cols], sum)
  names(df_prik) <- c(cols, 'prik')

  df <- merge(x = df_w, y = df_prik, by = cols)
  #df$w <- aggregate(data_stage$w, by=data_stage[cols], sum)$V1
  #df$prik <- aggregate(data_stage$prik, by=data_stage[cols], sum)$V1
  df$z <- df$eta + (df[, censor_col] - df$prik)/df$w
  df$num <- df$w*df[,coord]*(df$z - (df$eta - df[, coord]*beta[coord_index]))
  df$den <-  df$w*df[,coord]**2
  num <- sum(df$num)
  den <- sum(df$den)

  return(list(num = num, den = den))
}



